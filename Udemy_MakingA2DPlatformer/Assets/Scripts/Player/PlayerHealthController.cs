using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealthController : MonoBehaviour
{
    public static PlayerHealthController instance;
    private SpriteRenderer theSR;

    [Header("Health")]
    public int currentHealth;
    public int maxHealth;

    [Header ("Invincible")]
    [SerializeField] private float invincibleLength;
    private float invincibleCounter;

    [Header ("Effect")]
    public GameObject deathEffect;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        maxHealth = PlayerPrefs.GetInt("MaxHealth");
        currentHealth = PlayerPrefs.GetInt("CurrentHealth") > 0 ? PlayerPrefs.GetInt("CurrentHealth") : PlayerPrefs.GetInt("MaxHealth");
        theSR = GetComponent<SpriteRenderer>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(invincibleCounter > 0)
        {
            invincibleCounter -= Time.deltaTime;

            if(invincibleCounter <= 0)
            {
                theSR.color = new Color(theSR.color.r, theSR.color.g, theSR.color.b, 1f);
            }
        }
    }

    public void dealDamage()
    {
        if (invincibleCounter <= 0)
        {
           currentHealth--;
           PlayerPrefs.SetInt("CurrentHealth", currentHealth);
           if (currentHealth <= 0)
           {
                currentHealth = 0;
                Instantiate(deathEffect, transform.position, transform.rotation);
                
                LevelManager.instance.respawnPlayer();
           }
           else
           {
                invincibleCounter = invincibleLength;
                theSR.color = new Color(theSR.color.r, theSR.color.g, theSR.color.b, .5f);
                PlayerController.instance.knockBack();
           }
           UI_HealthBarController.instance.UpdateHealthDisplay(false, currentHealth);
        }
    }

    public void healPlayer()
    {
        if(currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
        else
        {
            ++currentHealth;
            PlayerPrefs.SetInt("CurrentHealth", currentHealth);
            UI_HealthBarController.instance.UpdateHealthDisplay(true, currentHealth - 1);
        }
    }
}
