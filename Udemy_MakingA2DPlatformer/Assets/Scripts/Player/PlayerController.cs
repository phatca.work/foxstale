using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    //public ParticleSystem thePS;

    public Animator anim;
    public Rigidbody2D theRB;
    private SpriteRenderer theSR;

    [Header("Moving")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpForce;
    private bool canDoubleJump;

    [Header("Check grounded")]
    [SerializeField] private bool isGrounded;
    public Transform groundCheckPoint;
    public LayerMask whatIsGrounded;

    [Header("Knockback")]
    [SerializeField] private float knockBackLength;
    [SerializeField] private float knockBackForce;
    private float knockBackCounter;

    public bool stopInput;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        theSR = GetComponent<SpriteRenderer>();
        theRB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Moving();
    }

    private void Moving()
    {
        if (knockBackCounter < 0)
        {
            isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, .35f, whatIsGrounded);
            anim.SetBool("isGrounded", isGrounded);
            checkFalling();
            if (!stopInput)
            {
                theRB.velocity = new Vector2(moveSpeed * Input.GetAxis("Horizontal"), theRB.velocity.y);
                anim.SetFloat("moveSpeed", Mathf.Abs(theRB.velocity.x));
                if (theRB.velocity.x < 0)
                {
                    theSR.flipX = true;
                    //createDust();
                }
                else if (theRB.velocity.x > 0)
                {
                    theSR.flipX = false;
                    //createDust();
                }
                jumping();
            }
        }
        else
        {
            knockBackCounter -= Time.deltaTime;
        }
    }

    private void jumping()
    {
        if (Input.GetButtonDown("Jump"))
        {
            anim.SetBool("Jumping", true);
            if (isGrounded)
            {
                theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
                canDoubleJump = true;
                AudioController.instance.playSFX("Player Jump");
            }
            else if (canDoubleJump)
            {
                canDoubleJump = false;
                anim.SetBool("Falling", false);
                theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
                AudioController.instance.playSFX("Player Jump");
                //createDust();
            }
        }
    }

    private void checkFalling()
    {
        if (anim.GetBool("Jumping"))
        {
            if (theRB.velocity.y < 0.1f || isGrounded)
            {
                anim.SetBool("Falling", true);
            }
        }
        if (isGrounded && anim.GetBool("Falling"))
        {
            anim.SetBool("Falling", false);
        }
    }

    public void knockBack()
    {
        knockBackCounter = knockBackLength;
        //_ = theSR.flipX ? new Vector2(knockBackForce, knockBackForce * 1.5f) : new Vector2(-knockBackForce, knockBackForce * 1.5f);
        if (theSR.flipX)
        {
            theRB.velocity = new Vector2(knockBackForce, knockBackForce * 1.5f);
        }
        else
        {
            theRB.velocity = new Vector2(-knockBackForce, knockBackForce * 1.5f);
        }
        AudioController.instance.playSFX("Player Hurt");
        anim.SetTrigger("Hurt");
        anim.SetBool("Falling", false);
    }

    public void bounceForce()
    {
        anim.SetBool("Falling", false);
        theRB.velocity = new Vector2(moveSpeed, jumpForce);
    }

    /*private void createDust()
    {
        thePS.Play();
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy_Damaged"))
        {
            if (anim.GetBool("Falling"))
            {
                collision.GetComponentInParent<EnemyController>().Death();
                return;
            }
        }

        if (collision.TryGetComponent(out ITouched itouched))
        {
            itouched.Touched();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out IExit iexit))
        {
            iexit.Exited();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy_Attack"))
        {
            GetComponent<PlayerHealthController>().dealDamage();
        }
        else if (collision.TryGetComponent(out IStay istay))
        {
            istay.Stayed();
        }
    }

}
