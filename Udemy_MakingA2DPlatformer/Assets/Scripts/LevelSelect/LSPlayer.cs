using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class LSPlayer : MonoBehaviour
{
    public MapPoint currentPoint;
    public PointLevel PL;
    public float moveSpeed = 10f;
    private bool levelLoading;
    public LSManager LSManager;
    public AudioSource selectMap;
    public AudioSource mapMovement;
    // Update is called once per frame

    void FixedUpdate()
    {
        PL = currentPoint.PL;
        transform.position = Vector3.MoveTowards(transform.position, currentPoint.transform.position, moveSpeed * Time.deltaTime);
        
        if(Vector3.Distance(transform.position, currentPoint.transform.position) < .1f && !levelLoading)
        {
            if (Input.GetAxisRaw("Horizontal") > 0.5f)
            {
                if(currentPoint.right != null)
                {
                    setNextPoint(currentPoint.right);
                    mapMovement.Play();
                }
            }
            else if (Input.GetAxisRaw("Horizontal") < -0.5f)
            {
                if (currentPoint.left != null)
                {
                    setNextPoint(currentPoint.left);
                    mapMovement.Play();
                }
            }
            else if (Input.GetAxisRaw("Vertical") > .5f)
            {
                if (currentPoint.up != null)
                {
                    setNextPoint(currentPoint.up);
                    mapMovement.Play();
                }
            }
            else if (Input.GetAxisRaw("Vertical") < -0.5f)
            {
                if (currentPoint.down != null)
                {
                    setNextPoint(currentPoint.down);
                    mapMovement.Play();
                }
            }
        }
        if(PL is not null)
        {
            if(transform.position != currentPoint.transform.position)
            {
                return;
            }
            if (PL.levelToLoad != "" && !PL.isLocked)
            {
                LSUI.instance.showInfor(PL);
                if (Input.GetButton("Enter"))
                {
                    Debug.Log("Enter");
                    levelLoading = true;
                    LSManager.LoadLevel();
                    selectMap.Play();
                }
            }
        }
        
    }

    public void setNextPoint(MapPoint nextPoint)
    {
        currentPoint = nextPoint;
        PL = currentPoint.PL;
        LSUI.instance.hideInfor();
    }
}
