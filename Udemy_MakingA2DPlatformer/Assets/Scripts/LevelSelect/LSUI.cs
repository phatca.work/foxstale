using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LSUI : MonoBehaviour
{
    public static LSUI instance;

    [Header("Fade screen")]
    public GameObject fade;
    public Image fadeScreen;
    public float fadeSpeed;
    private bool shouldFadeToBlack;
    private bool shouldFadeFromBlack;

    public GameObject levelInforPanel;
    public TMP_Text levelName; //, bestTime, timeTarget;
    public GameObject gemsPurple;
    public TMP_Text gemsPurpleInLevel, gemsPurpleTarget;
    public GameObject gemsGreen;
    public TMP_Text gemsGreenInLevel, gemsGreenTarget;
    public GameObject gemsRed;
    public TMP_Text gemsRedInLevel, gemsRedTarget;

    private RectTransform theRT;


    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        theRT = GetComponent<RectTransform>();
        fade.SetActive(true);
        fadeFromBlack();
    }

    private void FixedUpdate()
    {
        if (shouldFadeToBlack)
        {
            fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b,
                Mathf.MoveTowards(fadeScreen.color.a, 1f, fadeSpeed * Time.deltaTime));

            if (fadeScreen.color.a == 1f)
            {
                shouldFadeToBlack = false;
            }
        }

        if (shouldFadeFromBlack)
        {
            fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b,
                Mathf.MoveTowards(fadeScreen.color.a, 0f, fadeSpeed * Time.deltaTime));

            if (fadeScreen.color.a == 0f)
            {
                shouldFadeToBlack = false;
            }
        }
    }

    public void fadeToBlack()
    {
        shouldFadeToBlack = true;
        shouldFadeFromBlack = false;
    }

    public void fadeFromBlack()
    {
        shouldFadeToBlack = false;
        shouldFadeFromBlack = true;
    }

    public void showInfor(PointLevel levelInfor)
    {
        levelName.text = levelInfor.levelName;
        if(levelInfor.gemsPurpleTotal != 0)
        {
            gemsGreen.SetActive(true);
            gemsPurpleInLevel.text = "" + levelInfor.gemsPurpleCollected;
            gemsPurpleTarget.text = "" + levelInfor.gemsPurpleTotal;
        }
        else
        {
            gemsPurple.SetActive(false);
        }

        if(levelInfor.gemsGreenTotal != 0)
        {
            gemsGreen.SetActive(true);
            gemsGreenInLevel.text = "" + levelInfor.gemsGreenCollected;
            gemsGreenTarget.text = "" + levelInfor.gemsGreenTotal;
        }
        else
        {
            gemsGreen.SetActive(false);
        }

        if(levelInfor.gemsRedTotal != 0)
        {
            gemsRed.SetActive(true);
            gemsRedInLevel.text = "" + levelInfor.gemsRedCollected;
            gemsRedTarget.text = "" + levelInfor.gemsRedTotal;
        }
        else
        {
            gemsRed.SetActive(false);
        }

        theRT.anchoredPosition = new Vector2(levelInfor.transform.position.x, levelInfor.transform.position.y + 3f);

        //timeTarget.text = "TARGET: " + levelInfor.targetTime.ToString("F2") + "s";
        //bestTime.text = levelInfor.bestTime == 0 ? "BEST: ___" : "BEST: " + levelInfor.bestTime.ToString("F2") + "s";

        levelInforPanel.SetActive(true);
    }

    public void hideInfor()
    {
        levelInforPanel.SetActive(false);
    }
}
