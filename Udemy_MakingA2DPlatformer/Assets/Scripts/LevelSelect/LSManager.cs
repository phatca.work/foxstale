using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LSManager : MonoBehaviour
{
    public LSPlayer LSplayer;

    public MapPoint[] allPoints;

    private void Start()
    {
        allPoints = FindObjectsOfType<MapPoint>();
        if (PlayerPrefs.HasKey("currentLevel"))
        {
            foreach (MapPoint point in allPoints)
            {
                if (point.PL is not null)
                {
                    if (point.PL.levelToLoad == PlayerPrefs.GetString("currentLevel"))
                    {
                        LSplayer.transform.position = point.transform.position;
                        LSplayer.currentPoint = point;
                        break;
                    }
                }
            }
        }
    }

    public void LoadLevel()
    {
        StartCoroutine(LoadLevelCO());
    }

    public IEnumerator LoadLevelCO()
    {
        LSUI.instance.fadeToBlack();
        yield return new WaitForSeconds((1f / LSUI.instance.fadeSpeed) * 0.25f);
        SceneManager.LoadScene(LSplayer.currentPoint.PL.levelToLoad);
    }
}
