using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPoint : MonoBehaviour
{
    public MapPoint up;
    public MapPoint down;
    public MapPoint right;
    public MapPoint left;
    public PointLevel PL;
    private void Awake()
    {
        PL = GetComponent<PointLevel>();
    }
}
