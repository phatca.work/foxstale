using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointLevel : MonoBehaviour
{
    private SpriteRenderer theSR;
    public Sprite imageLevel;
    public string levelToLoad;
    public bool isLocked;
    public string levelToCheck;
    public string levelName;
    public int gemsPurpleCollected, gemsPurpleTotal;
    public int gemsGreenCollected, gemsGreenTotal;
    public int gemsRedCollected, gemsRedTotal;
    //public float bestTime, targetTime;

    private void Start()
    {
        theSR = gameObject.GetComponent<SpriteRenderer>();
        

        if (levelToLoad != null)
        {
            /*if (PlayerPrefs.HasKey(levelToLoad + "_gemsInLevel"))
            {
                totalGems = PlayerPrefs.GetInt(levelToLoad + "_gemsInLevel");
            }*/
            gemsPurpleCollected = PlayerPrefs.GetInt(levelToLoad + "_gemsPurpuleCollected");
            if (PlayerPrefs.HasKey(levelToLoad + "_gemsPurpuleCollected"))
            {
                gemsPurpleCollected = PlayerPrefs.GetInt(levelName + "_gemsPurpuleCollected");
            }

            for (int i = 1; ; i++)
            {
                if (PlayerPrefs.HasKey("GemGreen_" + levelName + "_" + i))
                {
                    gemsGreenCollected++;
                }
                else
                {
                    break;
                }
            }

            for (int i = 1; ; i++)
            {
                if (PlayerPrefs.HasKey("GemRed_" + levelName + "_" + i))
                {
                    gemsRedCollected++;
                }
                else
                {
                    break;
                }
            }

            /*if (PlayerPrefs.HasKey(levelToLoad + "_time"))
            {
                targetTime = PlayerPrefs.GetFloat(levelToLoad + "_time");
            }

            if (PlayerPrefs.HasKey(levelToLoad + "_bestTime"))
            {
                bestTime = PlayerPrefs.GetFloat(levelToLoad + "_bestTime");
            }*/


            isLocked = true;
            if (levelToCheck != null)
            {
                if (PlayerPrefs.HasKey(levelToCheck + "_unlocked"))
                {
                    if (PlayerPrefs.GetInt(levelToCheck + "_unlocked") == 1)
                    {
                        isLocked = false;
                        theSR.sprite = imageLevel;
                    }
                }
            }
            if (levelToLoad == levelToCheck)
            {
                isLocked = false;
                theSR.sprite = imageLevel;
            }
        }
    }
}
