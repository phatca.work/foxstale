using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpossumController : EnemyController
{
    private Vector2 check;
    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, .2f, whatIsGrounded);
        if (isGrounded)
        {
            Moving();
        }
    }

    public override void Moving()
    {
        bool isWall = Physics2D.OverlapCircle(facePoint.position, .2f, whatIsGrounded);
        if (isWall || theRB.velocity.x == 0)
        {
            movingRight = !movingRight;
        }


        if (leftPoint != 0 && transform.position.x < leftPoint)
        {
            movingRight = false;
        }

        if (rightPoint != 0 && transform.position.x > rightPoint)
        {
            movingRight = true;
        }

        if (movingRight)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            theRB.velocity = new Vector2(-moveSpeed, 0f);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            theRB.velocity = new Vector2(moveSpeed, 0f);
        }
    }
}
