using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FrogController : EnemyController
{
    [Header("Control moving")]
    [SerializeField] private float jumpHeight;
    [SerializeField] private float jumpLength;

    private void FixedUpdate()
    {
        JumpAndFallAnim();
    }

    private void JumpAndFallAnim()
    {
        if (anim.GetBool("Jumping"))
        {
            if (theRB.velocity.y < 0.1f)
            {
                anim.SetBool("Falling", true);
                anim.SetBool("Jumping", false);
            }
        }
        if (coll.IsTouchingLayers(whatIsGrounded) && anim.GetBool("Falling"))
        {
            anim.SetBool("Falling", false);
        }
    }
    public override void Moving()
    {
        //base.Moving();
        var isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, .2f, whatIsGrounded);
        if (isGrounded)
        {
            if (movingRight)
            {
                if (transform.position.x > leftPoint)
                {

                    theSR.flipX = false;

                    theRB.velocity = new Vector2(-jumpHeight, jumpLength);

                    anim.SetBool("Jumping", true);
                }
                else
                {
                    movingRight = false;
                }
            }
            else
            {
                if (transform.position.x < rightPoint)
                {

                    theSR.flipX = true;

                    theRB.velocity = new Vector2(jumpHeight, jumpLength);

                    anim.SetBool("Jumping", true);
                }
                else
                {
                    movingRight = true;
                }
            }
        }
    }
}
