using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoController : EnemyController
{
    public override void Moving()
    {
        bool isWall = Physics2D.OverlapCircle(facePoint.position, .2f, whatIsGrounded);
        if (isWall || theRB.velocity.x == 0)
        {
            movingRight = movingRight ? false : true;
        }

        if (leftPoint != 0 && transform.position.x < leftPoint)
        {
            movingRight = false;
        }

        if (rightPoint != 0 && transform.position.x > rightPoint)
        {
            movingRight = true;
        }

        if (movingRight)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            theRB.velocity = new Vector2(-moveSpeed, 0f);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            theRB.velocity = new Vector2(moveSpeed, 0f);
        }

    }

    private void FollowPlayer()
    {
        if (transform.position.x > PlayerController.instance.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            theRB.velocity = new Vector2(-moveSpeed * 2.5f, 0f);
        }
        else if (transform.position.x < PlayerController.instance.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            theRB.velocity = new Vector2(moveSpeed * 2.5f, 0f);
        }
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, .2f, whatIsGrounded);
        if (isGrounded)
        {
            if(Physics2D.OverlapBox(transform.position, new Vector2(25f, 25f), 0f, LayerMask.GetMask("Player")))
            {
                FollowPlayer();
            }
            else
            {
                Moving();
            }
        }
    }
}
