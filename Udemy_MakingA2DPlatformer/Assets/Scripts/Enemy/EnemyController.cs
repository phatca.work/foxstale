using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyController : MonoBehaviour
{
    protected Rigidbody2D theRB;
    protected SpriteRenderer theSR;
    protected Collider2D coll;
    protected Animator anim;

    public float moveSpeed;

    [Header("Range moving (X-axis)")]
    [SerializeField] protected float leftPoint;
    [SerializeField] protected float rightPoint;
    [SerializeField] protected bool movingRight;


    [Header("Checked")]
    [SerializeField] protected bool isGrounded;
    public Transform facePoint;
    public Transform groundCheckPoint;
    public LayerMask whatIsGrounded;

    [Header("Item drop rate")]
    [Range(0, 100)] public float chaceToDrop = 70;

    void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
        theSR = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();
        movingRight = true;
    }

    public abstract void Moving();

    public void Death()
    {
        gameObject.SetActive(false);
        Instantiate(Resources.Load("Prefabs/Effects/DeathEffect"), transform.position, transform.rotation);
        AudioController.instance.playSFX("Enemy Explode");
        PlayerController.instance.bounceForce();

        float dropSelect = Random.Range(1, 100);

        if (dropSelect <= chaceToDrop)
        {
            Instantiate(Resources.Load("Prefabs/Collections/Cherry"), transform.position, transform.rotation);
        }
    }
}