using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowXPlayer : MonoBehaviour
{
    [SerializeField] private Transform target;

    private void FixedUpdate()
    {
        transform.position = new Vector3(target.transform.position.x, transform.position.y, 0f);
    }
}
