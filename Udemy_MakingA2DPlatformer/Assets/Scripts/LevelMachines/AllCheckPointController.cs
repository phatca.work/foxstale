using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllCheckPointController : MonoBehaviour
{
    public static AllCheckPointController Instance;

    [Header ("List checkPoints")]
    private CheckPointController[] arrCheckPoint;

    [Header ("Current checkPoint")]
    public Vector3 spawnPoint;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        arrCheckPoint = FindObjectsOfType<CheckPointController>();
        spawnPoint = PlayerController.instance.transform.position;
    }

    public void DeactivateCheckPoint()
    {
        for(int i = 0; i < arrCheckPoint.Length; i++)
        {
            arrCheckPoint[i].ResetCheckPoint();
        }
    }

    //public void setSpawnPoint(Vector3 newSpawnPoint) => spawnPoint = newSpawnPoint;
}
