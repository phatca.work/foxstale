using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointController : MonoBehaviour, ITouched
{
    private SpriteRenderer theSR;

    [Header ("Sprite checkPoint")]
    public Sprite cpOn;
    public Sprite cpOff;
    // Start is called before the first frame update
    void Start()
    {
        theSR = GetComponent<SpriteRenderer>();
    }

    public void ResetCheckPoint() => theSR.sprite = cpOff;

    public void Touched()
    {
        if (theSR.sprite == cpOff)
        {
            AudioController.instance.playSFX("Check Point");
        }
        AllCheckPointController.Instance.DeactivateCheckPoint();
        theSR.sprite = cpOn;
        AllCheckPointController.Instance.spawnPoint = transform.position;
        
    }
}
