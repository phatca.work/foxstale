using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour, ITouched
{
    public Door door;

    private SpriteRenderer theSR;

    [SerializeField] Sprite downSprite;

    public bool hadSwitched;

    public void Touched()
    {
        if(!hadSwitched)
        {
            theSR.sprite = downSprite;
            hadSwitched = true;
            AudioController.instance.playSFX("Switch");
            door.checkSwitch();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        theSR = GetComponent<SpriteRenderer>();
    }
}
