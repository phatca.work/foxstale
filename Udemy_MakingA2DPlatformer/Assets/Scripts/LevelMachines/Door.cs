using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private Switch[] arrSwitch;

    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in arrSwitch)
        {
            item.door = this;
        }
    }

    public bool checkSwitch()
    {
        foreach (var item in arrSwitch)
        {
            if(!item.hadSwitched)
            {
                return false;
            }
        }
        gameObject.SetActive(false);
        return true;
    }
}
