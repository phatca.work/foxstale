using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    [Header("Collections")]
    public Text textGem;
    public Text textPointLives;

    [Header("Fade screen")]
    public GameObject fade;
    public Image fadeScreen;
    public float fadeSpeed;
    private bool shouldFadeToBlack;
    private bool shouldFadeFromBlack;

    [Header("Level manage")]
    public GameObject text_levelComplete;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        fade.SetActive(true);
        FadeFromBlack();
        UpdatePointLives();
    }

    private void FixedUpdate()
    {
        if(shouldFadeToBlack)
        {
            fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b,
                Mathf.MoveTowards(fadeScreen.color.a, 1f, fadeSpeed * Time.deltaTime));

            if(fadeScreen.color.a == 1f)
            {
                shouldFadeToBlack = false;
            }
        }

        if (shouldFadeFromBlack)
        {
            fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b,
                Mathf.MoveTowards(fadeScreen.color.a, 0f, fadeSpeed * Time.deltaTime));

            if (fadeScreen.color.a == 0f)
            {
                shouldFadeToBlack = false;
            }
        }
    }

    public void UpdatePointGem()
    {
        textGem.text = CollectionController.instance.gemsPurpuleCollected.ToString();
    }

    public void UpdatePointLives()
    {
        textPointLives.text = "x " + PlayerPrefs.GetInt("PointLives");
    }

    public void FadeToBlack()
    {
        shouldFadeToBlack = true;
        shouldFadeFromBlack = false;
    }

    public void FadeFromBlack()
    {
        shouldFadeToBlack = false;
        shouldFadeFromBlack = true;
    }
}
