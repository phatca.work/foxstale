using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UI_HealthBarController : MonoBehaviour
{
    public static UI_HealthBarController instance;
    public List<Image> arrHearth;
    
    [Header("Hearth sprite")]
    public Sprite heartFull;
    public Sprite heartEmpty;
    public Sprite heartHalf;
    [Space(1)]
    [SerializeField] private GameObject parentHearth;
    private GameObject hearth;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        hearth = (GameObject)Resources.Load("Prefabs/UI/Heart");
        CreateHealthBar();
    }
    public void CreateHealthBar()
    {
        float x = -300f;
        for (int i = 0; i < PlayerHealthController.instance.maxHealth / 2; i++)
        {
            GameObject newHearth = Instantiate(hearth, new Vector3(x, 0f), Quaternion.identity);
            newHearth.transform.SetParent(parentHearth.transform, false);
            x += 125f;
        }

        arrHearth = gameObject.GetComponentsInChildren<Image>().ToList();
        var currentHealth = PlayerPrefs.GetInt("CurrentHealth");
        for (int i = 0; i < arrHearth.Count; i++)
        {
            if ((currentHealth - 2) >= 0)
            {
                arrHearth[i].sprite = heartFull;
                currentHealth -= 2;
            }
            else if ((currentHealth - 1) == 0)
            {
                arrHearth[i].sprite = heartHalf;
                currentHealth -= 2;
            }
            else
            {
                arrHearth[i].sprite = heartEmpty;
            }
        }
    }
    public void UpdateFullHealthDisplay()
    {
        for (int i = 0; i < arrHearth.Count; i++)
        {
            arrHearth[i].sprite = heartFull;
        }
    }

    public void UpdateHealthDisplay(bool status, int health)
    {
        _ = health % 2 != 0 ?
        (arrHearth[health / 2].sprite = status ? heartFull : heartHalf) :
        (arrHearth[health / 2].sprite = status ? heartHalf : heartEmpty);
    }

    public void UpdateMaxHealth()
    {
        GameObject newHearth = Instantiate(hearth, new Vector3((PlayerHealthController.instance.maxHealth / 2 - 1) * 125f - 300f, 0f), Quaternion.identity);
        newHearth.transform.SetParent(parentHearth.transform, false);
        newHearth.GetComponent<Image>().sprite = heartEmpty;
        arrHearth.Add(newHearth.GetComponent<Image>());
    }
}
