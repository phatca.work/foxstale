using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    public float height, weight;

    [Header("Target character")]
    public Transform target;
    public bool stopFollow;

/*    [Header("Target background")]
    public Transform farBG;
    public Transform middleBG;*/

    private Vector2 lastPos;

    [Header("Camera limit (Y-axis)")]
    public float minHeight;
    public float maxHeight;

    private void Start()
    {
        lastPos = transform.position;
    }

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!stopFollow)
        {
            transform.position = new Vector3(target.position.x + height, Mathf.Clamp(target.position.y, minHeight, maxHeight) + weight, transform.position.z);
            Vector2 amountToMove = new Vector2(transform.position.x - lastPos.x, transform.position.y - lastPos.y);


            /*farBG.position += new Vector3(amountToMove.x, amountToMove.y, 0f);
            middleBG.position += new Vector3(amountToMove.x, amountToMove.y, 0f) * 0.5f;*/

            /*if(target.position.y > farBG.position.y)
            {
                farBG.position += new Vector3(amountToMove.x, target.position.y, 0f);
            }
            else
            {
                farBG.position += new Vector3(amountToMove.x, 0f, 0f);
            }*/

            lastPos = transform.position;
        }
    }


}
