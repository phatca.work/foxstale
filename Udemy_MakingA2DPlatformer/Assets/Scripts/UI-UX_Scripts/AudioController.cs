using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController instance;

    [Header ("List audio")]
    public AudioSource[] soundEffect;
    [Range (0, 100)]
    public float volume;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        soundEffect = gameObject.GetComponentsInChildren<AudioSource>();
    }

    public void playSFX(int soundToPlay)
    {
        if (soundToPlay >= 0 && soundToPlay < soundEffect.Length)
        {
            var sound = soundEffect[soundToPlay];
            sound.volume = volume / 100;
            sound.pitch = Random.Range(.8f, 1.2f);
            sound.Play();
        } 
        else
        {
            Debug.LogError("ERROR!!! SoundToPlay out range of array soundEffect");
        }
    }

    public void playSFX(string nameSoundToPlay)
    {
        int i = 0;
        foreach (var item in soundEffect)
        {
            if (item.name.Equals(nameSoundToPlay))
            {
                break;
            }
            i++;
        }

        if (i >= 0 && i < soundEffect.Length)
        {
            var sound = soundEffect[i];
            sound.volume = volume / 100;
            sound.pitch = Random.Range(.8f, 1.2f);
            sound.Play();
        }
        else
        {
            Debug.LogError("ERROR!!! SoundToPlay out range of array soundEffect");
        }
    }
}
