using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class MagnifylingGlass : MonoBehaviour, IStay, IExit
{
    private float maxSizeCamera = 7.5f;
    private float minSizeCamera = 5f;
    [SerializeField] private Camera mainCamera;
    private void FixedUpdate()
    {
        ZoomCamera();
        /*if(maxSizeCamera == 0)
        {
            maxSizeCamera = 7.5f;
        }
        if(minSizeCamera == 0)
        {
            minSizeCamera = 5f;
        }*/
    }

    public void ZoomCamera()
    {
        if (Input.GetKey(KeyCode.F) && MagnifylingGlassController.instance.stay)
        {
            if(Camera.main.orthographicSize > maxSizeCamera)
            {
                return;
            }

            Camera.main.orthographicSize += 0.1f;
            PlayerController.instance.GetComponent<Light2D>().intensity += 0.01f;
            PlayerController.instance.GetComponent<Light2D>().pointLightInnerRadius += 0.2f;
            PlayerController.instance.GetComponent<Light2D>().pointLightOuterRadius += 0.2f;
            foreach (var item in MagnifylingGlassController.instance.arrHideMap)
            {
                item.Stayed();
            }
        }
        else
        {
            if (Camera.main.orthographicSize <= minSizeCamera)
            {
                return;
            }
            Camera.main.orthographicSize -= 0.1f;
            PlayerController.instance.GetComponent<Light2D>().intensity -= 0.01f;
            PlayerController.instance.GetComponent<Light2D>().pointLightInnerRadius -= 0.2f;
            PlayerController.instance.GetComponent<Light2D>().pointLightOuterRadius -= 0.2f;
            foreach (var item in MagnifylingGlassController.instance.arrHideMap)
            {
                item.Exited();
            }
        }
    }

    public void Stayed()
    {
        MagnifylingGlassController.instance.stay = true;
    }

    public void Exited()
    {
        MagnifylingGlassController.instance.stay = false;
    }
}
