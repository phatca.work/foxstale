using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaFlag : MonoBehaviour, ITouched
{
    public void Touched()
    {
        LevelManager.instance.EndLevel();
        AudioController.instance.playSFX("End Game");
    }
}
