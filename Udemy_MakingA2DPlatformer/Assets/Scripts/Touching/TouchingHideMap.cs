using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TouchingHideMap : MonoBehaviour, IStay, IExit
{
    private Tilemap theTM;

    private void Start()
    {
        theTM = gameObject.GetComponent<Tilemap>();
    }

    public void Stayed()
    {
        theTM.color = new Color(theTM.color.r, theTM.color.g, theTM.color.b, 0.1f);
    }

    public void Exited()
    {
        theTM.color = new Color(theTM.color.r, theTM.color.g, theTM.color.b, 1f);
    }
}
