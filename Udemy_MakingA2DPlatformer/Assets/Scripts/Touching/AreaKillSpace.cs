using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaKillSpace : MonoBehaviour, ITouched
{
    public Transform target;
    public float number;

    public void Touched()
    {
        LevelManager.instance.respawnPlayer();
    }

    private void FixedUpdate()
    {
        transform.position = new Vector3(target.position.x - number, transform.position.y, transform.position.z);
    }
}
