using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour, ITouched, IExit
{
    public Transform[] Points;
    public float moveSpeed;
    public int startPoint;
    public int currentPoint;

    // Update is called once per frame  
    void FixedUpdate()
    {
        if (Vector2.Distance(transform.position, Points[currentPoint].position) < 0.05f)
        {
            currentPoint++;

            if (currentPoint >= Points.Length)

                currentPoint = 0;
        }
        transform.position = Vector2.MoveTowards(transform.position, Points[currentPoint].position, moveSpeed * Time.deltaTime);
    }

    public void Touched()
    {
        PlayerController.instance.transform.parent = transform;
    }

    public void Exited()
    {
        PlayerController.instance.transform.SetParent(null);
    }
}
