using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncePad : MonoBehaviour, ITouched
{
    private Animator anim;

    public float bounceForce = 20f;


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Touched()
    {
        PlayerController.instance.theRB.velocity = new Vector2(PlayerController.instance.theRB.velocity.x, bounceForce);
        PlayerController.instance.anim.SetBool("Falling", false);
        AudioController.instance.playSFX("Bouncer");
        anim.SetBool("Bounce", true);
    }

    public void NotTouch()
    {
        anim.SetBool("Bounce", false);
    }
}
