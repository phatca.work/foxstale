using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cherry : Collection, ITouched
{
    public void Touched()
    {
        PlayerHealthController.instance.healPlayer();
        AudioController.instance.playSFX("Pickup Health");
        Picked();
    }
}
