using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemPurple : Collection, ITouched
{
    public void Touched()
    {
        CollectionController.instance.gemsPurpuleCollected++;
        UIController.instance.UpdatePointGem();
        AudioController.instance.playSFX("Pickup Gem");
        Picked();
    }
}
