using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemGreen : CollectionLimited, ITouched
{
    public void Touched()
    {
        SetDataThisCollection();
        AudioController.instance.playSFX("Pickup Health");
        PlayerPrefs.SetInt("PointLives", PlayerPrefs.GetInt("PointLives") + 1);
        UIController.instance.UpdatePointLives();
        Picked();
    }
}
