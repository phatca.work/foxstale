using UnityEngine;

public class GemRed : CollectionLimited, ITouched
{
    public void Touched()
    {
        Picked();
        AudioController.instance.playSFX("Pickup Health");
        SetDataThisCollection();
        PlayerHealthController.instance.maxHealth += 2;
        PlayerPrefs.SetInt("MaxHealth", PlayerHealthController.instance.maxHealth);
        UI_HealthBarController.instance.UpdateMaxHealth();
    }
}
