using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionUnderWater : MonoBehaviour, ITouched
{
    public GameObject Real;

    public void Touched()
    {
        if(Real.TryGetComponent(out ITouched touch))
        {
            touch.Touched();
        }
    }
}
