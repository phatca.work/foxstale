using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectionController : MonoBehaviour
{
    public static CollectionController instance;
    [Header("Number of gems collected")]
    public int gemsPurpuleCollected;
    //public List<GemRed> gemRedCollected;
    public GemRed[] arrGemRed;
    public GemGreen[] arrGemGreen;


    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        SetNameGemLimited();
    }

    /*public void RespawnGemRed()
    {
        foreach (GemRed item in gemRedCollected)
        {
            item.gameObject.SetActive(true);
        }
        gemRedCollected.Clear();
    }

    public void PickedGemRed()
    {
        for (int i = 0; i < gemRedCollected.Count; i++)
        {
            
            PlayerPrefs.SetInt(gemRedCollected[i].GetComponent<CollectionLimited>().GetNameThisCollection(), 1);
        }
    }*/

    public void SetInforGemInLevelSelect(string nameScene)
    {
        //sua lai ham nay
        if (PlayerPrefs.HasKey(nameScene + "_gemsPurpuleCollected"))
        {
            if (gemsPurpuleCollected > PlayerPrefs.GetInt(nameScene + "_gemsPurpuleCollected"))
            {
                PlayerPrefs.SetInt(nameScene + "_gemsPurpuleCollected", gemsPurpuleCollected);
            }
        }
        else
        {
            PlayerPrefs.SetInt(nameScene + "_gemsPurpuleCollected", gemsPurpuleCollected);
        }
    }

    public void SetNameGemLimited()
    {
        arrGemRed = FindObjectsOfType<GemRed>();
        arrGemGreen = FindObjectsOfType<GemGreen>();
        for (int i = 0; i < arrGemRed.Length; i++)
        {
            arrGemRed[i].nameCollection = "GemRed_" + SceneManager.GetActiveScene().name + "_" + (i + 1);
            arrGemRed[i].CheckPicked();
        }
        for (int j = 0; j < arrGemGreen.Length; j++)
        {
            arrGemGreen[j].nameCollection = "GemGreen_"  + SceneManager.GetActiveScene().name + "_" + (j + 1);
            arrGemGreen[j].CheckPicked();
        }
    }
}
