using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectionLimited : Collection
{
    public string nameCollection;
    private string nameScene;
    // Start is called before the first frame update
    void Start()
    {
        nameScene = SceneManager.GetActiveScene().name;
    }

    public void CheckPicked()
    {
        if (PlayerPrefs.HasKey(nameCollection))
        {
            gameObject.SetActive(false);
        }
    }

    public void SetDataThisCollection()
    {
        PlayerPrefs.SetInt(nameCollection, 1);
    }

    public string GetNameThisCollection()
    {
        return nameCollection;
    }
}
