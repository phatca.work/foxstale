using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collection : MonoBehaviour
{

    public virtual void Picked()
    {
        gameObject.SetActive(false);
        Instantiate(Resources.Load("Prefabs/Effects/PickupEffect"), transform.position, transform.rotation);
    }
}
