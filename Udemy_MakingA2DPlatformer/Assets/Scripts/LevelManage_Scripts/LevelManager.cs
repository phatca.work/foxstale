using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    [Header ("Time respawn")]
    public float waitToRespawn;

    [Header ("Name's scene")]
    public string levelToLoad;

    public float timeInLevel;
    public bool stopTime;

    private void Update()
    {
        if (!stopTime)
        {
            timeInLevel += Time.deltaTime;
        }
    }

    private void Start()
    {
        timeInLevel = 0;
        stopTime = false;
    }
    private void Awake()
    {
        instance = this;
    }

    public void respawnPlayer()
    {
        PlayerPrefs.SetInt("PointLives", PlayerPrefs.GetInt("PointLives") - 1);
        //AudioController.instance.playSFX("Warp Jingle");
        if (PlayerPrefs.GetInt("PointLives") <= 0)
        {
            newGame();
        }
        else
        {
            StartCoroutine(respawnCo());
        }
    }
    private IEnumerator respawnCo()
    {
        PlayerController.instance.gameObject.SetActive(false); //x�a player

        yield return new WaitForSeconds(waitToRespawn - (1f / UIController.instance.fadeSpeed));

        UIController.instance.FadeToBlack();

        yield return new WaitForSeconds((1f / UIController.instance.fadeSpeed) + 0.2f);

        UIController.instance.FadeFromBlack();
        UIController.instance.UpdatePointLives();
        PlayerPrefs.SetInt("CurrentHealth", PlayerPrefs.GetInt("MaxHealth"));
        PlayerController.instance.gameObject.SetActive(true);
        PlayerController.instance.transform.position = AllCheckPointController.Instance.spawnPoint;
        PlayerHealthController.instance.currentHealth = PlayerHealthController.instance.maxHealth;
        UI_HealthBarController.instance.UpdateFullHealthDisplay();
    }

    public void newGame()
    {
        StartCoroutine(NewGameCo());
    }
    public IEnumerator NewGameCo()
    {
        PlayerController.instance.gameObject.SetActive(false);
        AudioController.instance.playSFX(3);
        PlayerPrefs.DeleteAll();
        yield return new WaitForSeconds(waitToRespawn - (1f / UIController.instance.fadeSpeed));

        UIController.instance.FadeToBlack();

        yield return new WaitForSeconds((1f / UIController.instance.fadeSpeed) + 0.2f);

        UIController.instance.FadeFromBlack();
        SceneManager.LoadScene("Main_menu");
    }

    public void EndLevel()
    {
        StartCoroutine(EndLevelCo());
    }
    public IEnumerator EndLevelCo()
    {
        stopTime = true;
        PlayerController.instance.stopInput = true;

        CameraController.instance.stopFollow = true;

        UIController.instance.text_levelComplete.SetActive(true);

        yield return new WaitForSeconds(1.5f);

        UIController.instance.FadeToBlack();

        yield return new WaitForSeconds((1f / UIController.instance.fadeSpeed) + 0.2f);

        

        LoadDataPlayerPrefs();
        
        SceneManager.LoadScene(levelToLoad);
    }

    private void LoadDataPlayerPrefs()
    {
        var nameScene = SceneManager.GetActiveScene().name;

        PlayerPrefs.SetInt(nameScene + "_unlocked", 1);

        PlayerPrefs.SetString("currentLevel", nameScene);

        CollectionController.instance.SetInforGemInLevelSelect(nameScene);
    }
}
