using UnityEngine;

public class FarMapFollowPlayer : MonoBehaviour
{
    public Transform player;
    private Vector2 lastPos;
    private void Start()
    {
        lastPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 amountToMove = new Vector2(player.position.x - lastPos.x, player.position.y - lastPos.y);

        transform.position += new Vector3(amountToMove.x, 0f);

        if(player.position.y > 3)
        {
            transform.position += new Vector3(0f, amountToMove.y);
        }
        else
        {
            if(transform.position.y > 0)
            {
                transform.position += new Vector3(0f, -0.1f);
            }
        }

        lastPos = transform.position;
    }
}
