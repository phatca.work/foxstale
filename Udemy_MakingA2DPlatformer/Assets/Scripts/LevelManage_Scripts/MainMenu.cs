using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour {

    [Header ("Name's scene start game")]
    public string startGame;
    public string loadContinueGame;
    public GameObject continueButton;
    public GameObject startButton;
    private AudioSource theAS;

    private void Start()
    {
        theAS = GetComponent<AudioSource>();
        if (PlayerPrefs.HasKey("currentLevel"))
        {
            continueButton.SetActive(true);
            startButton.transform.position = new Vector3(0, -175, 0);
        }
        else
        {
            continueButton.SetActive(false);
            startButton.GetComponentInChildren<TMP_Text>().text = "NEW GAME";
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(startGame);
        InformationForNewGame();
        theAS.Play();
    }

    private void InformationForNewGame()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("PointLives", 3);
        PlayerPrefs.SetInt("MaxHealth", 2);
        PlayerPrefs.SetInt("CurrentHealth", 2);
    }

    public void ContinueGame()
    {
        SceneManager.LoadScene(loadContinueGame);
        theAS.Play();
    }

    public void QuitGame()
    {
        Application.Quit();
        theAS.Play();
    }
}
