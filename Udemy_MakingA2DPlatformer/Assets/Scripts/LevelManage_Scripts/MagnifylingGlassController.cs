using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MagnifylingGlassController : MonoBehaviour
{
    public static MagnifylingGlassController instance;

    

    [SerializeField] public bool stay;
    [SerializeField] public List<TouchingHideMap> arrHideMap;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        arrHideMap = FindObjectsOfType<TouchingHideMap>().ToList();
    }
}
