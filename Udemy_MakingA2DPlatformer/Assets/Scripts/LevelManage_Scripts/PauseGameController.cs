using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGameController : MonoBehaviour
{
    public string scene_MainMenu;
    public string scene_SelectLevel;
    [SerializeField]
    private AudioSource soundBackground;

    public GameObject panel_PauseGame;
    public bool isPause;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Resume();
        }
    }

    public void Resume()
    {
        if (isPause)
        {
            isPause = false;
            panel_PauseGame.SetActive(false);
            Time.timeScale = 1.0f;
            soundBackground.Play();

        }
        else
        {
            isPause = true;
            panel_PauseGame.SetActive(true);
            Time.timeScale = 0f;
            soundBackground.Pause();
        }
    }

    public void SelectLevel()
    {
        PlayerPrefs.GetString("CurrentLevel", SceneManager.GetActiveScene().name);
        panel_PauseGame.SetActive(false);
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(scene_SelectLevel);
    }

    public void BackMainMenu()
    {
        panel_PauseGame.SetActive(false);
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(scene_MainMenu);
    }
}
