using UnityEngine;

public abstract class IVictim : MonoBehaviour
{
    public int currentHealth;
    public int maxHealth;
    public bool died;
    public HealthBar healthBar;
    public float timeHurt;

    public virtual void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    public virtual void TakeDamage(int dmg)
    {   
        currentHealth -= dmg;
        healthBar.SetHealth(currentHealth);
    }
    protected virtual void Die()
    {
        died = true;
        healthBar.gameObject.transform.parent.gameObject.SetActive(false);
    }
    public virtual void KnockBack(Vector2 thrust)
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(thrust.x, thrust.y);
    }

    public abstract void Damaged(int dmg, Vector2 thrust);
}
