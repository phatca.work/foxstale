using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IAttacker : MonoBehaviour
{
    public int attackDamage;
    public Vector2 thrust;
    public int rateCritical;

    protected virtual bool Attacking(Collider2D enemy)
    {
        if (enemy.TryGetComponent(out IVictim victim))
        {
            if (victim.currentHealth <= 0) return true;
            victim.Damaged(attackDamage, new Vector2(transform.rotation.y == 0f ? thrust.x : -thrust.x, thrust.y));
            PoolDmgPoint.instance.GetObjectFromPool(enemy.transform.position, attackDamage, Random.Range(0, 100) < rateCritical);
        }
        return false;
    }
}
