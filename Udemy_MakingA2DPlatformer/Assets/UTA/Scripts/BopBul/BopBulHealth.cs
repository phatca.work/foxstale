using UnityEngine;

public class BopBulHealth : IVictim
{
    private Animator anim;

    public override void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
        anim.SetFloat("Hurting", timeHurt);
    }
    public override void KnockBack(Vector2 thrust)
    {
        base.KnockBack(thrust);
    }

    protected override void Die()
    {
        base.Die();
        anim.SetTrigger("Dying");

    }

    public override void Damaged(int dmg, Vector2 thrust)
    {
        TakeDamage(dmg);
        KnockBack(thrust);
        GetComponent<BopBulController>().ResetTimeMove();
        if (currentHealth <= 0) Die();
    }

    private void FixedUpdate()
    {
        if (anim.GetFloat("Hurting") >= 0)
        {
            anim.SetFloat("Hurting", anim.GetFloat("Hurting") - Time.deltaTime);
        }
    }
}
