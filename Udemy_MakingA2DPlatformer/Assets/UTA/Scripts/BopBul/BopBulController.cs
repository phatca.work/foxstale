using UnityEngine;

public class BopBulController : MonoBehaviour
{
    protected Rigidbody2D theRB;
    protected SpriteRenderer theSR;
    protected Animator anim;


    [Header("Particle Control")]
    [SerializeField] private ParticleSystem thePS;
    [Range(0, 0.5f)]
    [SerializeField] float dustFormationPeriod;
    float counter;

    public float moveSpeed;

    [Header("Range moving (X-axis)")]
    [SerializeField] protected float leftPoint;
    [SerializeField] protected float rightPoint;
    [SerializeField] protected bool movingRight;

    [SerializeField] private float timeMove;
     public float timeMoving;
    


    [Header("Checked")]
    [SerializeField] protected bool isGrounded;
    public Transform facePoint;
    public Transform groundCheckPoint;
    public LayerMask whatIsGrounded;

    [SerializeField] private Transform player;

    public Vector2 checkZone;

    void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
        theSR = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        movingRight = true;
    }

    private void Moving()
    {
        if(timeMoving >= 0)
        {
            bool isWall = Physics2D.OverlapCircle(facePoint.position, .2f, whatIsGrounded);
            if (isWall/* || theRB.velocity.x == 0*/)
            {
                movingRight = !movingRight;
            }

            if (leftPoint != 0 && transform.position.x < leftPoint)
            {
                movingRight = false;
            }

            if (rightPoint != 0 && transform.position.x > rightPoint)
            {
                movingRight = true;
            }

            if (movingRight)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
                theRB.velocity = new Vector2(-moveSpeed, 0f);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
                theRB.velocity = new Vector2(moveSpeed, 0f);
            }
            timeMoving -= Time.deltaTime;
            if(counter > dustFormationPeriod)
            {
                thePS.Play();
                counter = 0;
            }
        }
        anim.SetFloat("MoveSpeed", Mathf.Abs(theRB.velocity.x));
    }

    /*private void FollowPlayer()
    {
        if (transform.position.x > player.position.x + 0.1f)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            theRB.velocity = new Vector2(-moveSpeed, 0f);
        }
        else if (transform.position.x < player.position.x - 0.1f)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            theRB.velocity = new Vector2(moveSpeed, 0f);
        }
        else
        {
            theRB.velocity = new Vector2(0f, 0f);
        }
    }*/

    void FixedUpdate()
    {
        counter += Time.deltaTime;
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, .2f, whatIsGrounded);
        if (isGrounded)
        {
            /*if (Physics2D.OverlapBox(transform.position, new Vector2(25f, 25f), 0f, LayerMask.GetMask("Player")))
            {
                FollowPlayer();
                return;
            }
            */
            if (GetComponent<IVictim>().died) return;
            if (anim.GetFloat("Hurting") >= 0) return;
            var victim = Physics2D.OverlapBox(transform.position, checkZone, 0f, LayerMask.GetMask("Player"));
            if (victim != null && victim.GetComponent<IVictim>().currentHealth > 0)
            {
                anim.SetBool("Attacking", true);
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                return;
            }
            else
            {
                anim.SetBool("Attacking", false);
            }
            if (anim.GetBool("Attacking")) return;
            //if (anim.GetBool("Attacking")) return;
            Moving();
        }
        
    }

    public void ResetTimeMove()
    {
        timeMoving = timeMove;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(transform.position, checkZone);
    }
}
