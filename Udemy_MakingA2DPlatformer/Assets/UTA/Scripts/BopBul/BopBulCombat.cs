using UnityEngine;

public class BopBulCombat : IAttacker
{
    private Animator anim;
    public LayerMask enemyLayers;
    [SerializeField] private Transform attackPoint;

    public float attackZone;
    
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void StartAttack()
    {
        Collider2D[] enemies = Physics2D.OverlapCircleAll(attackPoint.position, attackZone, enemyLayers);
        var a = enemies;
        foreach (Collider2D enemy in enemies)
        {
            Attacking(enemy);
        }
    }

    protected override bool Attacking(Collider2D enemy)
    {
        base.Attacking(enemy);
        return true;
    }
    private void EndAttack()
    {
        anim.SetBool("Attacking", false);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(attackPoint.position, attackZone);
    }
}
