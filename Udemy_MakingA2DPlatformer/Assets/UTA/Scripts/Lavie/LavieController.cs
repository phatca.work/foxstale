using System;
using UnityEngine;

public class LavieController : MonoBehaviour
{
    private Animator anim;
    private Rigidbody2D theRB;

    public bool stopInput;

    [Header("Check grounded")]
    [SerializeField] public Collider2D isGrounded;
    public Transform groundCheckPoint;
    public LayerMask whatIsGrounded;

    [Header("Movement")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpForce;
    private bool canDoubleJump;

    [Header("MoveSpeed change when touching")]
    [Range(0, 1)] public float MSOnWater;


    [Header("Knockback")]
    public float knockBackCounter;

    public int huongdi;
    public ParticleSystem dust;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        theRB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<IVictim>().died) return;
        if (GetComponent<LavieCombat>().attacking) return;
        if (knockBackCounter >= 0)
        {
            knockBackCounter -= Time.deltaTime;
            anim.SetFloat("Hurting", knockBackCounter);
            return;
        }
        Moving();
    }

    public float CheckWhichGroundToSlow()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, .35f, whatIsGrounded);
        if (isGrounded == null) return moveSpeed;
        return isGrounded.tag switch
        {
            "Water" => MSOnWater * moveSpeed,
            _ => moveSpeed,
        };
    }

    private void SetAnimationStop(float horizontal)
    {
        if ((Input.GetAxisRaw("Horizontal") == 0 && theRB.velocity.x == 0))
        {
            //CreateDust();
            anim.SetBool("Stop", true);
        }
        else
        {
            anim.SetBool("Stop", false);
        }
    }

    private void SetAnimationTurnBack(float horizontal)
    {
        if (horizontal == 0 && theRB.velocity.x == 0)
        {
            huongdi = 0;
        }
        if ((huongdi == 1 && horizontal < 0f) && isGrounded)
        {
            anim.SetBool("turnBack", true);
        }
        else if ((huongdi == -1 && horizontal > 0f) && isGrounded)
        {
            anim.SetBool("turnBack", true);
        }
    }

    private void Transfer(float horizontal)
    {
        theRB.velocity = new Vector2(CheckWhichGroundToSlow() * horizontal, theRB.velocity.y);
        anim.SetFloat("moveSpeed", Mathf.Abs(theRB.velocity.x));

        if (theRB.velocity.x < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            huongdi = -1;
        }
        else if (theRB.velocity.x > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            huongdi = 1;
        }
    }

    private void jumping()
    {
        if (Input.GetButtonDown("Jump"))
        {
            anim.SetBool("Jumping", true);
            if (isGrounded)
            {
                theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
                canDoubleJump = true;
                //AudioController.instance.playSFX("Player Jump");
            }
            else if (canDoubleJump)
            {
                anim.SetTrigger("doubleJump");
                canDoubleJump = false;
                //anim.SetBool("Falling", false);
                theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
                //AudioController.instance.playSFX("Player Jump");
            }
        }
    }

    private void Moving()
    {
        anim.SetBool("isGrounded", isGrounded != null);
        float horizontal = Input.GetAxis("Horizontal");
        SetAnimationTurnBack(horizontal);
        SetAnimationStop(horizontal);
        Transfer(horizontal);
        jumping();
    }

    private void CreateDust()
    {
        dust.Play();
    }

    public void setBoolTurnBack()
    {
        anim.SetBool("turnBack", false);
    }
}
