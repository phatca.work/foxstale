using UnityEngine;

public class LavieHealth : IVictim
{
    private Animator anim;
    private LavieController theLC;
    private Rigidbody2D theRB;

    public override void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
        theLC = GetComponent<LavieController>();
        theRB = GetComponent<Rigidbody2D>();
    }

    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        anim.SetFloat("Hurting", theLC.knockBackCounter);
    }

    protected override void Die()
    {
        base.Die();
        theRB.sharedMaterial = null;
        theRB.velocity = new Vector2(transform.rotation.y == 0f ? -5f : 5f, 5f);
        anim.SetTrigger("Die");
        anim.SetFloat("Hurting", 0f);
    }

    public override void KnockBack(Vector2 thrust)
    {
        base.KnockBack(thrust);
        theLC.knockBackCounter = timeHurt;
    }

    public override void Damaged(int dmg, Vector2 thrust)
    {
        TakeDamage(dmg);
        KnockBack(thrust);
        if (currentHealth <= 0) Die();
    }
}
