using UnityEngine;

public class LavieCombat : IAttacker
{
    private Animator anim;

    public GameObject attackEffect;

    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;

    public float timeSleepOfAttack = 2f;
    public float nextAttackTime = 0f;
    public bool attacking;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (nextAttackTime <= 0)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                StartAttack();
                nextAttackTime = timeSleepOfAttack;
            }
        }
        else
        {
            nextAttackTime -= Time.deltaTime;
        }
    }

    private void StartAttack()
    {
        anim.SetTrigger("Attack");
        attacking = true;
        if(TryGetComponent(out LavieController item) && item.isGrounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(item.CheckWhichGroundToSlow() * transform.rotation.y == 0f ? 1f : -1f, 0f);
        }
    }

    protected override bool Attacking(Collider2D enemy)
    {
        if(base.Attacking(enemy)) return true;
        Instantiate(attackEffect, enemy.transform.position, Quaternion.identity);
        return false;
    }

    private void CreateEffectAttack()
    {
        attacking = false;
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
        foreach (Collider2D enemy in hitEnemies)
        {
            Attacking(enemy);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null) return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
