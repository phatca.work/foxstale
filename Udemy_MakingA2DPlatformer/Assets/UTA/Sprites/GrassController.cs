using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassController : MonoBehaviour
{
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;
        anim.SetBool(collision.transform.rotation.y == 0 ? "leftToRight" : "rightToLeft", true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        anim.SetBool("leftToRight", false);
        anim.SetBool("rightToLeft", false);
    }
}
