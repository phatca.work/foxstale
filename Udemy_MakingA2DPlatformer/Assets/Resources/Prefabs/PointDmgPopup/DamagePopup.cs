using UnityEngine;
using TMPro;

public class DamagePopup : MonoBehaviour
{
    private TextMeshPro textMesh;
    private float disappearTimer;
    private Color textColor;
    private const float DISAPPEAR_TIME_MAX = 1f;
    [SerializeField] private int fontSize;

    private Vector3 moveVector;
    private static int sortingOrder;

    private void Awake()
    {
        textMesh = transform.GetComponent<TextMeshPro>();
    }

    public void Setup(int dmgAmount, bool isCriticalHit)
    {
        textMesh.SetText("-" + dmgAmount.ToString());

        if(!isCriticalHit)
        {
            textMesh.fontSize = fontSize;
            //textMesh.color = new Color(1, 0.1367925f, 0.1367925f);
        }
        else
        {
            textMesh.fontSize = fontSize * 2f;
            //textMesh.color = new Color(1, 0.8128629f, 0.1367925f);
        }
        sortingOrder++;
        textMesh.sortingOrder = sortingOrder;
        textColor = textMesh.color;
        disappearTimer = DISAPPEAR_TIME_MAX;
        moveVector = new Vector3(Random.Range(-2f, 2f), Random.Range(0f, 2f), 0f) * 10f;
    }

    public DamagePopup Create(Transform dmgPopupTransform, int dmgAmount, bool isCriticalHit)
    {
        DamagePopup dmgPopup = dmgPopupTransform.GetComponent<DamagePopup>();

        dmgPopup.Setup(dmgAmount, isCriticalHit);
        return dmgPopup;
    }

    private void Update()
    {
        transform.position += moveVector * Time.deltaTime;
        moveVector -= moveVector * 8f * Time.deltaTime;

        if(disappearTimer > DISAPPEAR_TIME_MAX * 0.5f)
        {
            float increaseScaleAmount = 1f;
            transform.localScale += Vector3.one * increaseScaleAmount * Time.deltaTime;
        }
        else
        {
            float decreaseScaleAmount = 1f;
            transform.localScale -= Vector3.one * decreaseScaleAmount * Time.deltaTime;
        }
        disappearTimer -= Time.deltaTime;
        if(disappearTimer < 0)
        {
            float disappearSpeed = 3f;
            textColor.a -= disappearSpeed * Time.deltaTime;
            textMesh.color = textColor;

            if(textColor.a < 0)
            {
                PoolDmgPoint.instance.RemoveObjectToPool(gameObject, textMesh);
            }
        }
    }
}
